package com.demo.mvptestmq.routes;

import com.demo.mvptestmq.data.Confirmation;
import com.demo.mvptestmq.data.Decision;
import org.apache.camel.Message;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;


@Component
public class JMSRoutes extends RouteBuilder {

    @Override
    public void configure() {
        from("jms:queue:HIPMVPGCMS")
                .unmarshal().json(JsonLibrary.Jackson, Decision.class)
                .log("MQ: Received decision with CorrelationID: ${headers.JMSCorrelationID} and body: ${body}.")
                .log("MQ: Processing decision...")
                .delay(3000)
                .bean(this, "createConfirmation")
                .log("MQ: Success! Sending confirmation to response queue")
                .to("jms:queue:RESPONSEQUEUE?requestTimeout=4500");

    }

    public void createConfirmation(Exchange exchange) {
        Message in = exchange.getIn();
        Confirmation confirmation = new Confirmation("Success", "Decision saved to GCMS");
        in.setBody(confirmation);
    }

}

