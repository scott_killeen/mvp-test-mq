package com.demo.mvptestmq.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Decision {

    private String applicationNumber;
    private String status;

}
